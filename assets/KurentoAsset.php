<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class KurentoAsset extends AssetBundle
{
    public $sourcePath = "@bower";
//    public $css = [
//        'css/site.css',
//    ];
    public $js = [
        'adapter.js/adapter.js',
        'jquery/dist/jquery.js',
        'kurento-utils/js/kurento-utils.js'
    ];
}
