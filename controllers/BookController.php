<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\BookSearch;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete', 'detail-view'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetailView($id){
        return $this->renderPartial("detail_view", [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //сохраняем картинку на диск
            $model->previewImage = UploadedFile::getInstance($model, 'previewImage');
            if(!empty($model->previewImage)) $model->previewImage->saveAs(Book::PREVIEW_UPLOAD_PATH . $model->preview);

            return $this->redirect(['book/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Book model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        //сохраняем предыдущий URL для сохранения пагинации, сортировки, фильтров
        if(Yii::$app->request->method == "GET"){
            Yii::$app->session->set("prevURL", Yii::$app->request->referrer);
        }

        $model = $this->findModel($id);
        $model->date = date("d.m.Y", $model->date);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //сохраняем картинку на диск
            $model->previewImage = UploadedFile::getInstance($model, 'previewImage');
            if(!empty($model->previewImage)) $model->previewImage->saveAs(Book::PREVIEW_UPLOAD_PATH . $model->preview);

            return $this->redirect(Yii::$app->session->get("prevURL"));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ErrorException
     */
    public function actionDelete($id)
    {
        //удаляем картинку
        $mBook = $this->findModel($id);
        //дефолтную картинку не удаляем
        if($mBook->preview != Book::NO_PHOTO_FILENAME){
            if(!unlink(Book::PREVIEW_UPLOAD_PATH . $mBook->preview)){
                throw new ErrorException("Can not delete file" . $mBook->preview);
            }
        }
        $mBook->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
