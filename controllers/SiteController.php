<?php

namespace app\controllers;

use app\models\UserModel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use PhpOffice\PhpWord\PhpWord;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect(['book/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionRegister() {
        $mUser = new UserModel();
        if($mUser->load(Yii::$app->request->post()) && $mUser->save()){
            //авторизуем пользователя
            $mLoginForm = new LoginForm();
            $mLoginForm->username = $mUser->username;
            Yii::$app->user->login($mLoginForm->getUser(), 3600*24*30);
            $this->redirect(['book/index']);
        } else {
            return $this->render('register', ['mUser' => $mUser]);
        }
    }

    public function actionTestWord(){

        $phpWord = new PhpWord();

        $template = $phpWord->loadTemplate("img/test.docx");

        $template->setValue("name", "Ivan");

        $template->saveAs("img/newtest.docx");

        echo "done!";
    }

}
