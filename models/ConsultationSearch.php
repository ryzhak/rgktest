<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Consultation;

/**
 * ConsultationSearch represents the model behind the search form about `app\models\Consultation`.
 */
class ConsultationSearch extends Consultation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date_create', 'date_update', 'teacher_id'], 'integer'],
            [['category_by_stream', 'category_by_count'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Consultation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $queryArray = [
            'id' => $this->id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ];
        /*$mUser = UserModel::findOne(Yii::$app->user->id);
        if($mUser->category == "teacher"){
            $queryArray['teacher_id'] = $mUser->id;
            $queryArray['id'] = $this->id;
        }
        if($mUser->category == "pupil"){
            if($mConsPupil = ConsultationPupil::findOne('pupil_id = :p_id',[
                ':p_id' => $mUser->id
            ])){
                $queryArray['id'] = $mConsPupil->consultation_id;
            }
        }*/
        $query->andFilterWhere($queryArray);

        $query->andFilterWhere(['like', 'category_by_stream', $this->category_by_stream])
            ->andFilterWhere(['like', 'category_by_count', $this->category_by_count]);

        return $dataProvider;
    }

}
