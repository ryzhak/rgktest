<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "consultation".
 *
 * @property integer $id
 * @property integer $date_create
 * @property integer $date_update
 * @property string $category_by_stream
 * @property string $category_by_count
 * @property integer $teacher_id
 *
 * @property Users $teacher
 * @property ConsultationPupil[] $consultationPupils
 */
class Consultation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'date_update', 'teacher_id'], 'integer'],
            [['category_by_stream', 'category_by_count'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'category_by_stream' => 'Category By Stream',
            'category_by_count' => 'Category By Count',
            'teacher_id' => 'Teacher ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Users::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsultationPupils()
    {
        return $this->hasMany(ConsultationPupil::className(), ['consultation_id' => 'id']);
    }

    public static function getDataProvider(){

        $mUser = UserModel::findOne(Yii::$app->user->id);

        if($mUser->category == "teacher"){
            $query = self::find()->where(['teacher_id' => $mUser->id]);
        }

        if($mUser->category == "pupil"){
            $query = self::find()->joinWith("consultationPupils")->where('pupil_id = :p_id',[
                ':p_id' => $mUser->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;

    }

}
