<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultation_pupil".
 *
 * @property integer $id
 * @property integer $consultation_id
 * @property integer $pupil_id
 * @property integer $date_create
 * @property integer $date_update
 *
 * @property Users $pupil
 * @property Consultation $consultation
 */
class ConsultationPupil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultation_pupil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['consultation_id', 'pupil_id', 'date_create', 'date_update'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'consultation_id' => 'Consultation ID',
            'pupil_id' => 'Pupil ID',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupil()
    {
        return $this->hasOne(Users::className(), ['id' => 'pupil_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsultation()
    {
        return $this->hasOne(Consultation::className(), ['id' => 'consultation_id']);
    }
}
