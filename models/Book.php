<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\Author;
use yii\web\UploadedFile;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_create
 * @property integer $date_update
 * @property string $preview
 * @property integer $date
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Book extends \yii\db\ActiveRecord
{

    const PREVIEW_UPLOAD_PATH = "img/previews/";
    const NO_PHOTO_FILENAME = "nophoto.jpg";

    public $previewImage;
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date'], 'required'],
            [['date_create', 'date_update', 'author_id'], 'integer'],
            [['date'],'date', 'format' => 'dd.mm.yyyy'],
            [['name', 'preview'], 'string', 'max' => 255],
            [['previewImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_create' => 'Дата добавления',
            'date_update' => 'Дата обновления',
            'preview' => 'Превью',
            'date' => 'Дата выхода книги',
            'author_id' => 'Автор',
            'previewImage' => 'Превью'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $previewImage = UploadedFile::getInstance($this, 'previewImage');
            //если создание книги
            if($insert){
                //если нет превью книги, то ставим дефолтную картинку
                if(empty($previewImage)){
                    $this->preview = self::NO_PHOTO_FILENAME;
                } else {
                    $this->preview = yii::$app->security->generateRandomString(24) . "." . $previewImage->extension;
                }
            } else {
                //при обновлении
                if(!empty($previewImage)){
                    $this->preview = yii::$app->security->generateRandomString(24) . "." . $previewImage->extension;
                }
            }

            $this->date = strtotime($this->date);

            return true;
        } else {
            return false;
        }
    }
}
