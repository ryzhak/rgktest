<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 */
class UserModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password', 'authKey', 'accessToken', 'full_name', 'category'], 'string', 'max' => 255],
            ['username', 'validateLogin']
        ];
    }

    /**
     * Валидация на одинаковый логин
     *
     * @param $attribute the attribute currently being validated
     * @param $params the additional name-value pairs given in the rule
     */
    public function validateLogin($attribute, $params){
        if(!$this->hasErrors() && self::isLoginUsed($this->username)){
            $this->addError($attribute, "Login is already used");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    //хешируем пароль, задаем токены для авторизации
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->password = md5($this->password);
            $this->authKey = "key-" . yii::$app->security->generateRandomString(24);
            $this->accessToken = "access-token-" . yii::$app->security->generateRandomString(24);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверяем используется ли данный логин
     *
     * @param $username логин
     * @return boolean используется ли данный логин
     */
    public function isLoginUsed($username){
        $isLoginUsed = false;
        if (self::findOne(['username' => $username])){
            $isLoginUsed = true;
        }
        return $isLoginUsed;
    }

    /**
     * Возвращает человеко-читаемую надпись времени, например "сегодня", "вчера", "2 дн.назад" и т.д.
     *
     * @param $oldTimestamp
     * @return string
     */
    public static function getHumanTime($oldTimestamp){
        $secInDay = 24 * 60 * 60;
        $now = time();
        $days = ($now - $oldTimestamp) / $secInDay;
        if($days < 1){
            return "Сегодня";
        }
        if($days < 2){
            return "Вчера";
        }
        return round($days) . " дн. назад";
    }

}
