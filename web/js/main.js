$(document).ready(function() {

    //добавляем лайтбокс для превью
    var $lightbox = $('#lightbox');

    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'),
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });

    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');

        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });

    //подгружаем вьюху детального просмотра при нажатии на глаз
    var $modalDetailView = $('#modalDetailView');
    $modalDetailView.on('shown.bs.modal', function (e) {
        $lightbox.find('.close').removeClass('hidden');
    });
    $(".ajax-detail-view").click(function(){
        console.log('click');
        var modelID = $(this).data("modelId");
        $.ajax({
            method: "GET",
            url: "detail-view?id=" + modelID
        })
        .done(function( resp ) {
            $("#modalDetailView .modal-content").html(resp);
        });
    });

});