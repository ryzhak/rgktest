<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_142349_create_tables_books_authors extends Migration
{

    public function safeUp()
    {

        $this->createTable('authors',[
            'id' => 'pk',
            'firstname' => Schema::TYPE_STRING,
            'lastname' => Schema::TYPE_STRING
        ]);

        $this->createTable('books',[
            'id' => 'pk',
            'name' => Schema::TYPE_STRING,
            'date_create' => Schema::TYPE_INTEGER,
            'date_update' => Schema::TYPE_INTEGER,
            'preview' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_INTEGER,
            'author_id' => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("FK_books_authors", 'books', 'author_id', 'authors', 'id', 'CASCADE', 'CASCADE');

        //вставляем тестовые данные в таблицу авторов
        $this->batchInsert('authors',['firstname','lastname'],[
            ["Борис", "Акунин"],
            ["Стивен", "Кинг"],
            ["Дэн","Браун"]
        ]);

        //вставляем тестовые данные в таблицу книг
        $this->batchInsert('books',['name','date_create', 'date_update','preview','date','author_id'],[
            ["Весь мир театр", 1438706844, 1438706844, "akunin1.jpg", 1104537600, 1],
            ["Нефритовые четки", 1438550444, 1438706844, "akunin2.jpg", 1104437600, 1],
            ["Смерть Ахиллеса", 1438406844, 1438706844, "akunin3.jpg", 1104337600, 1],
            ["Кэрри", 1438706844, 1438706844, "king1.jpg", 1104237600, 2],
            ["Долгая прогулка", 1438706844, 1438706844, "king2.jpg", 1104137600, 2],
            ["Сияние", 1438706844, 1438706844, "king3.jpg", 1104037600, 2],
            ["Инферно", 1438706844, 1438706844, "brown1.jpg", 1103937600, 3],
            ["Утраченный символ", 1438706844, 1438706844, "brown2.jpg", 1103937600, 3],
            ["Цифровая крепость", 1438706844, 1438706844, "brown3.jpg", 1103837600, 3],
        ]);

        $this->createTable('users',[
            'id' => 'pk',
            'username' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'authKey' => Schema::TYPE_STRING,
            'accessToken' => Schema::TYPE_STRING
        ]);
        $this->batchInsert('users',['username','password', 'authKey', 'accessToken'],[
            ["rgktest", md5("rgktest"), 'key-001', 'token-001'],
        ]);

    }
    
    public function safeDown()
    {
        $this->dropTable('books');
        $this->dropTable('authors');
        $this->dropTable('users');
    }

}
