<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_085023_add_psyline_tables extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('users', 'full_name', Schema::TYPE_STRING);
        //teacher, pupil
        $this->addColumn('users', 'category', Schema::TYPE_STRING);

        $this->createTable('consultation',[
            'id' => 'pk',
            'date_create' => Schema::TYPE_INTEGER,
            'date_update' => Schema::TYPE_INTEGER,
            'category_by_stream' => Schema::TYPE_STRING, //аудио, видео
            'category_by_count' => Schema::TYPE_STRING, //одиночное, групповое, конференция
            'teacher_id' => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("FK_consultation_users", 'consultation', 'teacher_id', 'users', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('consultation_pupil',[
            'id' => 'pk',
            'consultation_id' => Schema::TYPE_INTEGER,
            'pupil_id' => Schema::TYPE_INTEGER,
            'date_create' => Schema::TYPE_INTEGER,
            'date_update' => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("FK_pupil_consultation", 'consultation_pupil', 'consultation_id', 'consultation', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_pupil_user", 'consultation_pupil', 'pupil_id', 'users', 'id', 'CASCADE', 'CASCADE');

        //тестовые данные
        $this->batchInsert('users',['id', 'username','password', 'authKey', 'accessToken', 'full_name', 'category'],[
            [2, "teacher1", md5("teacher1"), 'key-0001', 'token-0001', 'ФИО teacher1', 'teacher'],
            [3, "teacher2", md5("teacher2"), 'key-0002', 'token-0002', 'ФИО teacher2', 'teacher'],
            [4, "pupil1", md5("pupil1"), 'key-0003', 'token-0003', 'ФИО pupil1', 'pupil'],
            [5, "pupil2", md5("pupil2"), 'key-0004', 'token-0004', 'ФИО pupil2', 'pupil'],
        ]);
        $this->batchInsert('consultation',['id', 'date_create','date_update', 'category_by_stream', 'category_by_count', 'teacher_id'],[
            [1, time(), time(), 'audio', 'single', 2],
            [2, time(), time(), 'video', 'single', 2],
        ]);
        $this->batchInsert('consultation_pupil',['id', 'consultation_id','pupil_id', 'date_create', 'date_update'],[
            [1, 1, 4, time(), time()],
            [2, 2, 4, time(), time()],
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('consultation_pupil');
        $this->dropTable('consultation');
        $this->dropColumn('users', 'category');
        $this->dropColumn('users', 'full_name');
        $this->delete('users','id > :id',[':id' => 1]);
    }
}
