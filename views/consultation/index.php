<?php

use app\models\UserModel;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsultationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultations';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- scripts used for broadcasting -->
<script src="//cdn.webrtc-experiment.com/firebase.js"></script>
<script src="//cdn.webrtc-experiment.com/RTCMultiConnection.js"></script>
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>

<div class="consultation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Consultation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_create',
            'date_update',
            'category_by_stream',
            'category_by_count',
            'teacher_id',
            [
                'attribute' => 'Статус',
                'format' => 'raw',
                'value' => function($model){
                    return "offline";
                }
            ],
            [
                'attribute' => 'Начать сессию',
                'format' => 'raw',
                'value' => function($model){
                    $mUser = UserModel::findOne(Yii::$app->user->id);
                    if($mUser->category == "teacher"){
                        return "<button data-confname='conference-{$model->id}' type='button' class='btn btn-primary setup-new-conference' data-toggle='modal' data-target='#myModal'>начать сессию</button>";
                    }
                    if($mUser->category == "pupil"){
                        return "<button data-confname='conference-{$model->id}' type='button' class='btn btn-primary connect-to-conference' data-toggle='modal' data-target='#myModal'>открыть сессию</button>";
                    }
                }
            ]
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<!-- модальное окно с видео -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Видео</h4>
            </div>
            <div id="videos-container" class="modal-body">

            </div>
        </div>
    </div>
</div>

<script>

    var connection = new RTCMultiConnection();
    connection.session = {
        audio: true,
        video: true
    };

    connection.onstream = function(e) {
        e.mediaElement.width = 600;
        videosContainer.insertBefore(e.mediaElement, videosContainer.firstChild);
        rotateVideo(e.mediaElement);
        scaleVideos();
    };

    function rotateVideo(mediaElement) {
        mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(0deg)';
        setTimeout(function() {
            mediaElement.style[navigator.mozGetUserMedia ? 'transform' : '-webkit-transform'] = 'rotate(360deg)';
        }, 1000);
    }

    connection.onstreamended = function(e) {
        e.mediaElement.style.opacity = 0;
        rotateVideo(e.mediaElement);
        setTimeout(function() {
            if (e.mediaElement.parentNode) {
                e.mediaElement.parentNode.removeChild(e.mediaElement);
            }
            scaleVideos();
        }, 1000);
    };


    //переделать
    /*var sessions = {};
    connection.onNewSession = function(session) {
        if (sessions[session.sessionid]) return;
        sessions[session.sessionid] = session;

        var tr = document.createElement('tr');
        tr.innerHTML = '<td><strong>' + session.sessionid + '</strong> is running a conference!</td>' +
        '<td><button class="join">Join</button></td>';
        roomsList.insertBefore(tr, roomsList.firstChild);

        var joinRoomButton = tr.querySelector('.join');
        joinRoomButton.setAttribute('data-sessionid', session.sessionid);
        joinRoomButton.onclick = function() {
            this.disabled = true;

            var sessionid = this.getAttribute('data-sessionid');
            session = sessions[sessionid];

            if (!session) throw 'No such session exists.';

            connection.join(session);
        };
    };*/

    $(".connect-to-conference").on("click", function(){
        $(this).disabled = true;
        connection.join($(this).data('confname'));
    });

    var videosContainer = document.getElementById('videos-container') || document.body;
    var roomsList = document.getElementById('rooms-list');

    /*document.getElementById('setup-new-conference').onclick = function() {
        this.disabled = true;
        connection.open(document.getElementById('conference-name').value || 'Anonymous');
    };*/

    //преподаватель инициализирует сессию
    $(".setup-new-conference").on("click", function(){
        $(this).disabled = true;
        //connection.open(document.getElementById('conference-name').value || 'Anonymous');
        connection.open($(this).data('confname'));
    });

    // setup signaling to search existing sessions
    //connection.connect();

    (function() {
        var uniqueToken = document.getElementById('unique-token');
        if (uniqueToken)
            if (location.hash.length > 2) uniqueToken.parentNode.parentNode.parentNode.innerHTML = '<h2 style="text-align:center;"><a href="' + location.href + '" target="_blank">Share this link</a></h2>';
            else uniqueToken.innerHTML = uniqueToken.parentNode.parentNode.href = '#' + (Math.random() * new Date().getTime()).toString(36).toUpperCase().replace(/\./g, '-');
    })();

    function scaleVideos() {
        var videos = document.querySelectorAll('video'),
            length = videos.length,
            video;

        var minus = 130;
        var windowHeight = 700;
        var windowWidth = 600;
        var windowAspectRatio = windowWidth / windowHeight;
        var videoAspectRatio = 4 / 3;
        var blockAspectRatio;
        var tempVideoWidth = 0;
        var maxVideoWidth = 0;

        for (var i = length; i > 0; i--) {
            blockAspectRatio = i * videoAspectRatio / Math.ceil(length / i);
            if (blockAspectRatio <= windowAspectRatio) {
                tempVideoWidth = videoAspectRatio * windowHeight / Math.ceil(length / i);
            } else {
                tempVideoWidth = windowWidth / i;
            }
            if (tempVideoWidth > maxVideoWidth)
                maxVideoWidth = tempVideoWidth;
        }
        for (var i = 0; i < length; i++) {
            video = videos[i];
            if (video)
                video.width = maxVideoWidth - minus;
        }
    }

    window.onresize = scaleVideos;

</script>
