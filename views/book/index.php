<?php

use app\models\Book;
use app\models\UserModel;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';

?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        "summary" => "",
        'columns' => [

            'id',
            'name',
            [
                'attribute' => 'preview',
                'format' => 'raw',
                'value' => function ($model) {
                    $imgSrc = "/" . Book::PREVIEW_UPLOAD_PATH . $model->preview;
                    return "<a href='#' class='thumbnail' data-toggle='modal' data-target='#lightbox'> " .
                            Html::img($imgSrc,['width' => 75]) .
                            "</a>";
                },
            ],
            [
                'attribute' => 'author_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return "{$model->author->firstname} {$model->author->lastname}";
                },
            ],
            [
                'attribute' => 'date',
                'format' => ['date','php:d.m.Y'],
            ],
            [
                'attribute' => 'date_create',
                'format' => 'raw',
                'value' => function ($model) {
                    return UserModel::getHumanTime($model->date_create);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{update} {detailView} {delete}",
                'buttons' => [
                    'detailView' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', "#", [
                            'title' => Yii::t('app', 'Info'),
                            'data-model-id' => $model->id,
                            "data-toggle" => "modal",
                            "data-target" => "#modalDetailView",
                            'class' => "ajax-detail-view"
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>

<!-- место для модального окна лайтбокса превью -->
<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>

<div id="modalDetailView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            detail
        </div>
    </div>
</div>
