<?php

use app\models\Author;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'Автор',
                'value' => "{$model->author->firstname} {$model->author->lastname}"
            ],
            [
                'attribute' => 'date_create',
                'format' => ['date','php:d.m.Y'],
            ],
            [
                'attribute' => 'date_update',
                'format' => ['date','php:d.m.Y'],
            ],
            [
                'attribute' => 'date',
                'format' => ['date','php:d.m.Y'],
            ],
        ],
    ]) ?>

</div>
