<?php

use app\models\Author;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4">

            <?= $form->field($model, 'author_id')->dropDownList(ArrayHelper::map(Author::find()->all(),'id','lastname'),['prompt' => 'Выбрать автора']) ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-md-4 text-right">

        </div>
    </div>
    <label class="control-label">Дата выхода книги:</label>
    <div class="row">
        <div class="col-md-8">
            <?php
                echo DatePicker::widget([
                    'model' => $model,
                    "language" => "ru",
                    'attribute' => 'dateFrom',
                    'attribute2' => 'dateTo',
                    'options' => ['placeholder' => 'Начало периода'],
                    'options2' => ['placeholder' => 'Конец периода'],
                    'type' => DatePicker::TYPE_RANGE,
                    'form' => $form,
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => true,
                    ]
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>







    <?php ActiveForm::end(); ?>

</div>
